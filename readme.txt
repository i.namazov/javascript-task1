Theoretical question

1) Explain in your own words the difference between declaring variables via var, let and const.

With the introduction of ES6, two important new JavaScript keywords were introduced as well: let and const. Before the introduction of ES6, the only way to declare a variable in JavaScript was by using var. But what is the difference between var, let, and const?
Var
We know that, the old way to declare a variable in JavaScript. When we create a variable with var and cannot assign any value, undefined is assigned to the value of the created variable by default. When we call this variable we created with console.log, we get the undefined output. Var is function scope, that’s why when we create var variable into the function, we use var it can be called and used in other functions in the function. But if we try to call out of the function, we get an error.

Let (var vs let)
First, let's compare the var and let. The difference between var and let has the var function scope while the let block scope property. That is why, a variable created with let is only accessible in ornate brackets where it was created and cannot be used outside.

function caramount(prices, addvalue) {
let newamountlist = [];
for (let i = 0; i < prices.length; i++) {
let finalprice = prices[i] + addvalue;
newpricelist.push(finalprice);
}
console.log(i)   // referenceerror: i is not defined
console.log(finalprice)
return newpricelist;
}
 
We get an error ReferenceError: i is not defined. The reason we got this is because let keyword is block scope. If we try to call a variable created with the let keyword outside of the curly brackets in which it was created, we get an error
With the var keyword, variables can be redefined and their values updated. We cannot redefine a variable defined with let, but we can update its value.


Const (let vs const)
A variable created with const cannot be reassigned later. We can later assign a new value to a variable we create with let, but a new value cannot be reassigned to the variable created with const. This situation is defined as immutable. Let's take a look at what kind of const is immutable and what is mutable.

const person = {
name: ‘ıbrahim namazov’
}
person.name = ‘ıbo ıbrahim namazov’
person = {}   // uncaught typeerror: assignment to constant variable.
 

Changing or adding a property within an object is not reassigning. The Immutable status is called the state of the object being unable to assign a new value to it.  Let and Const are block scope, they cannot be used outside the scope in anyway.
 


2). Why is declaration of a variable via `var` considered a bad tone?

There are different type of reason. Probably the first disadvantage of the var variable is, if we will be use  twice same variable, it will be not give the error,
but , if we will use declare the same variable twice with const and let, we will get the error message.
At the same time, for example, if we will write the var variable into the if statement or for  loop, and call variable with the same name in the other part of your code, then we will can be get value of this variable and it can be the problem for us.
Let's see one example;

var a = 20;
if (condition){
 // This is no error
var a = 21;
 }
 console.log(a);

The result of code will be 21 and it can be the problem for us.
